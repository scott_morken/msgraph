<?php

declare(strict_types=1);

use Microsoft\Graph\Beta\GraphServiceClient;
use Smorken\MsGraph\Constants\Scope;
use Smorken\MsGraph\Constants\UrlEndpoint;

return [
    'urls' => [
        UrlEndpoint::BASE->value => 'https://graph.microsoft.com',
        UrlEndpoint::API_ENDPOINT->value => 'https://graph.microsoft.com/beta/',
        UrlEndpoint::SCOPE_ENDPOINT->value => 'https://graph.microsoft.com/.default',
    ],
    'client_class' => GraphServiceClient::class,
    'client_id' => env('MS_GRAPH_CLIENT_ID', env('AZURE_CLIENT_ID')),
    'client_secret' => env('MS_GRAPH_CLIENT_SECRET', env('AZURE_CLIENT_SECRET')),
    'tenant' => env('MS_GRAPH_TENANT_ID', env('AZURE_TENANT_ID')),
    'proxy' => env('PROXY'),  // optionally
    'scopes' => env('MS_GRAPH_SCOPES', [Scope::USER_READ_ALL]),
];
