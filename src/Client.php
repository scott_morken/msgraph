<?php

declare(strict_types=1);

namespace Smorken\MsGraph;

use Microsoft\Graph\Beta\GraphServiceClient as BetaClient;
use Microsoft\Graph\Core\Authentication\GraphPhpLeagueAccessTokenProvider;
use Microsoft\Kiota\Authentication\Oauth\ClientCredentialContext;
use Microsoft\Kiota\Authentication\Oauth\TokenRequestContext;
use Smorken\MsGraph\Constants\Scope;
use Smorken\MsGraph\Constants\UrlEndpoint;
use Smorken\MsGraph\Support\Urls;

class Client implements \Smorken\MsGraph\Contracts\Client
{
    protected BetaClient $client;

    protected TokenRequestContext $credentialContext;

    protected ?string $token = null;

    public function __construct(
        protected string $clientClass,
        protected string $clientId,
        protected string $clientSecret,
        protected string $tenantId,
        protected Urls $urls,
        protected array $scopes = [Scope::USER_READ_ALL]
    ) {
        $this->init();
    }

    public function getClient(): BetaClient
    {
        return $this->client;
    }

    public function getToken(): string
    {
        if (! $this->token) {
            $provider = new GraphPhpLeagueAccessTokenProvider($this->credentialContext);
            $this->token = $provider->getAuthorizationTokenAsync($this->urls->get(UrlEndpoint::BASE))->wait();
        }

        return $this->token;
    }

    public function getUrls(): Urls
    {
        return $this->urls;
    }

    protected function createClient(): BetaClient
    {
        $clientClass = $this->clientClass;

        return new $clientClass($this->credentialContext, $this->getScopesEndpoint());
    }

    protected function createCredentialContext(): TokenRequestContext
    {
        return new ClientCredentialContext(
            $this->tenantId,
            $this->clientId,
            $this->clientSecret,
        );
    }

    protected function getScopes(): array
    {
        $scopes = [];
        foreach ($this->scopes as $scope) {
            if (is_a($scope, Scope::class)) {
                $scope = $scope->value;
            }
            $scopes[] = $scope;
        }

        return $scopes;
    }

    protected function getScopesEndpoint(): array
    {
        return [$this->urls->get(UrlEndpoint::SCOPE_ENDPOINT)];
    }

    protected function init(): void
    {
        $this->credentialContext = $this->createCredentialContext();
        $this->client = $this->createClient();
    }
}
