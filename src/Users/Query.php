<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Users;

use Microsoft\Graph\Beta\Generated\Models\User;
use Microsoft\Graph\Beta\Generated\Models\UserCollectionResponse;
use Microsoft\Graph\Beta\Generated\Users\UsersRequestBuilderGetQueryParameters;
use Microsoft\Graph\Beta\Generated\Users\UsersRequestBuilderGetRequestConfiguration;
use Smorken\MsGraph\Contracts\Filter;

/**
 * @template TRequest of Users
 *
 * @extends \Smorken\MsGraph\Query<TRequest>
 */
class Query extends \Smorken\MsGraph\Query
{
    protected UsersRequestBuilderGetRequestConfiguration $configuration;

    public function filter(Filter $filter): self
    {
        $this->getQueryParameters()->filter = (string) $filter;

        return $this;
    }

    public function find(string $userId): ?User
    {
        return $this->action()->byUserId($userId)->get()->wait();
    }

    public function get(): UserCollectionResponse
    {
        return $this->action()->get($this->getConfiguration())->wait();
    }

    public function getConfiguration(): UsersRequestBuilderGetRequestConfiguration
    {
        return $this->configuration;
    }

    public function getQueryParameters(): UsersRequestBuilderGetQueryParameters
    {
        return $this->configuration->queryParameters;
    }

    public function orderBy(array $fields): self
    {
        $this->getQueryParameters()->orderby = $fields;

        return $this;
    }

    public function search(?string $search): self
    {
        $this->getQueryParameters()->search = $search;

        return $this;
    }

    public function select(array $fields): self
    {
        $this->getQueryParameters()->select = $fields;

        return $this;
    }

    public function top(int $count): self
    {
        $this->getQueryParameters()->top = $count;

        return $this;
    }

    protected function initAction(): void
    {
        $this->action = $this->getRequest()->client()->getClient()->users();
    }

    protected function initQuery(): void
    {
        $this->configuration = new UsersRequestBuilderGetRequestConfiguration;
        $this->configuration->queryParameters = new UsersRequestBuilderGetQueryParameters;
    }
}
