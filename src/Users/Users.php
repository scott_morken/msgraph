<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Users;

use Smorken\MsGraph\Request;

/**
 * @extends Request<\Smorken\MsGraph\Users\Query<static>>
 */
class Users extends Request
{
    protected string $queryClass = Query::class;
}
