<?php

declare(strict_types=1);

namespace Smorken\MsGraph;

use Smorken\MsGraph\Contracts\Request;

/**
 * @template TRequest of Request
 */
abstract class Query implements \Smorken\MsGraph\Contracts\Query
{
    protected mixed $action = null;

    /**
     * @param  TRequest  $request
     */
    public function __construct(protected Request $request)
    {
        $this->initAction();
        $this->initQuery();
    }

    public function action(): mixed
    {
        return $this->action;
    }

    /**
     * @return TRequest
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    protected function initAction(): void
    {
        // override
    }

    protected function initQuery(): void
    {
        // override
    }
}
