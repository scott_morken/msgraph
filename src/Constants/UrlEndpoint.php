<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Constants;

enum UrlEndpoint: string
{
    case BASE = 'base';
    case API_ENDPOINT = 'api_endpoint';

    case SCOPE_ENDPOINT = 'scope_endpoint';
}
