<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Constants\Filter;

enum EqualityOperator: string
{
    case EQUAL = 'eq';
    case NOT_EQUAL = 'ne';
    case GREATER_THAN = 'gt';
    case GREATER_THAN_OR_EQUAL = 'ge';
    case LESS_THAN = 'lt';
    case LESS_THAN_OR_EQUAL = 'le';
    case IN = 'in';
    case NOT_IN = 'not in';
}
