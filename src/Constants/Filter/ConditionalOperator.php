<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Constants\Filter;

enum ConditionalOperator: string
{
    case AND = 'and';
    case OR = 'or';
}
