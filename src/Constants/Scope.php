<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Constants;

enum Scope: string
{
    case USER_READ = 'User.Read';
    case USER_READ_ALL = 'User.Read.All';
}
