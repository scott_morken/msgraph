<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Query;

use Smorken\MsGraph\Constants\Filter\ConditionalOperator;
use Smorken\MsGraph\Constants\Filter\EqualityOperator;
use Smorken\MsGraph\Query\Parts\FilterPart;
use Smorken\MsGraph\Query\Parts\StandardFunction;
use Smorken\MsGraph\Query\Parts\Where;

class Filter implements \Smorken\MsGraph\Contracts\Filter
{
    /**
     * @var FilterPart[]
     */
    protected array $parts = [];

    public function __toString(): string
    {
        $parts = [];
        foreach ($this->parts as $part) {
            $parts[] = (string) $part->part;
            $parts[] = Str::enumToString($part->operator);
        }
        array_pop($parts);

        return implode(' ', $parts);
    }

    public function endsWith(
        string $attribute,
        string $value,
        ConditionalOperator $operator = ConditionalOperator::AND
    ): self {
        $this->addStandardFunction('endsWith', $attribute, Str::wrap($value), $operator);

        return $this;
    }

    public function startsWith(
        string $attribute,
        string $value,
        ConditionalOperator $operator = ConditionalOperator::AND
    ): self {
        $this->addStandardFunction('startsWith', $attribute, Str::wrap($value), $operator);

        return $this;
    }

    public function where(
        string $attribute,
        string|int|float $value,
        EqualityOperator $equality = EqualityOperator::EQUAL,
        ConditionalOperator $conditional = ConditionalOperator::AND
    ): self {
        $w = new Where($attribute, $value, $equality);
        $this->parts[] = new FilterPart($w, $conditional);

        return $this;
    }

    protected function addStandardFunction(
        string $functionName,
        string $attribute,
        string $value,
        ConditionalOperator $operator
    ): void {
        $f = new StandardFunction($functionName, [$attribute, $value]);
        $this->parts[] = new FilterPart($f, $operator);
    }
}
