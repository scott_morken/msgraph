<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Query\Parts;

use Smorken\MsGraph\Constants\Filter\EqualityOperator;
use Smorken\MsGraph\Contracts\Part;
use Smorken\MsGraph\Query\Str;

class Where implements Part
{
    public function __construct(
        public string $attribute,
        public string|int|float $value,
        public EqualityOperator $equality = EqualityOperator::EQUAL,
    ) {}

    public function __toString(): string
    {
        return sprintf('%s %s %s', $this->attribute, Str::enumToString($this->equality), $this->getValue($this->value));
    }

    protected function getValue(string|int|float $value): string
    {
        $raw = ['null', 'false', 'true'];
        if (in_array($value, $raw, true)) {
            return $value;
        }
        if (is_numeric($value)) {
            return $value;
        }

        return Str::wrap($value);
    }
}
