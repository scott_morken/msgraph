<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Query\Parts;

use Smorken\MsGraph\Constants\Filter\ConditionalOperator;

class FilterPart
{
    public function __construct(
        public \Stringable $part,
        public ConditionalOperator $operator
    ) {}
}
