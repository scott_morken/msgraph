<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Query\Parts;

use Smorken\MsGraph\Contracts\Part;

class StandardFunction implements Part
{
    public function __construct(
        public string $functionName,
        public array $arguments = []
    ) {}

    public function __toString(): string
    {
        return sprintf('%s(%s)', $this->functionName, implode(',', $this->arguments));
    }
}
