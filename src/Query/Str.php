<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Query;

class Str
{
    public static function enumToString(string|\BackedEnum $v): string
    {
        return is_string($v) ? $v : $v->value;
    }

    public static function wrap(string $string): string
    {
        if (! str_starts_with($string, "'")) {
            $string = "'".$string;
        }
        if (! str_ends_with($string, "'")) {
            $string = $string."'";
        }

        return $string;
    }
}
