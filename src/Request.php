<?php

declare(strict_types=1);

namespace Smorken\MsGraph;

use Smorken\MsGraph\Contracts\Client;
use Smorken\MsGraph\Contracts\Query;

/**
 * @template TQuery of Query
 */
abstract class Request implements \Smorken\MsGraph\Contracts\Request
{
    protected static ?\Closure $clientResolver = null;

    protected ?Client $client = null;

    protected string $queryClass;

    public static function setClientResolver(\Closure $resolver): void
    {
        self::$clientResolver = $resolver;
    }

    public function __call(string $name, array $arguments)
    {
        return $this->newQuery()->$name(...$arguments);
    }

    public function client(): Client
    {
        if (! $this->client) {
            $method = self::$clientResolver;
            $this->client = $method();
        }

        return $this->client;
    }

    /**
     * @return TQuery
     */
    public function newQuery(): Query
    {
        return new $this->queryClass($this);
    }
}
