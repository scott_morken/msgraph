<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Scripts;

use Composer\Script\Event;
use Smorken\MsGraph\Scripts\Support\Copy;
use Smorken\MsGraph\Scripts\Support\FileMap;
use Smorken\MsGraph\Scripts\Support\Remove;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class Composer
{
    public static string $basePath = '/microsoft/microsoft-graph-beta/src/Generated/';

    public static ?Filesystem $filesystem = null;

    public static ?Finder $finder = null;

    public static function requiredOnly(Event $event)
    {
        $fileMap = new FileMap(self::$finder);
        $composer = $event->getComposer();
        $extra = $composer->getPackage()->getExtra();
        $package = $extra['ms-graph-package'] ?? 'microsoft-graph-beta';
        $requiredPath = __DIR__.'/../../required/'.$package.'/src';
        $requiredFiles = $fileMap->map($requiredPath);
        $event->getIO()->write("[$package] requires ".count($requiredFiles).' files/directories.');
        $vendorPath = $composer->getConfig()->get('vendor-dir').'/microsoft/'.$package.'/src';
        $vendorFiles = $fileMap->map($vendorPath);
        $event->getIO()->write("[$package] has ".count($vendorFiles).' files/directories.');
        $remove = new Remove(self::$filesystem);
        $removed = $remove->remove($requiredFiles, $vendorFiles);
        $event->getIO()->write("[$package] removed $removed files/directories.");
        $copy = new Copy(self::$filesystem);
        $copied = $copy->copy([
            __DIR__.'/stubs/user.stub' => $vendorPath.'/Generated/Models/User.php',
        ]);
        $event->getIO()->write("[$package] copied $copied files/directories.");
    }
}
