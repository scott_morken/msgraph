<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Scripts\Support;

use Symfony\Component\Finder\Finder;

class FileMap
{
    public function __construct(
        protected ?Finder $finder
    ) {
        if (! $this->finder) {
            $this->finder = new Finder;
        }
    }

    public function map(string $path): array
    {
        $files = [];
        foreach ($this->finder->in($path) as $file) {
            if (method_exists($file, 'isDot') && $file->isDot()) {
                continue;
            }
            $files[] = new File($file->getRealPath(), $file->isFile());
        }

        return $files;
    }
}
