<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Scripts\Support;

use Symfony\Component\Filesystem\Filesystem;

class Copy
{
    public function __construct(protected ?Filesystem $filesystem = null)
    {
        if (! $this->filesystem) {
            $this->filesystem = new Filesystem;
        }
    }

    public function copy(array $map): int
    {
        $copied = 0;
        foreach ($map as $from => $to) {
            if ($this->filesystem->exists($from)) {
                $this->filesystem->copy($from, $to, true);
                $copied++;
            }
        }

        return $copied;
    }
}
