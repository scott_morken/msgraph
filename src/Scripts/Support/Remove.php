<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Scripts\Support;

use Symfony\Component\Filesystem\Filesystem;

class Remove
{
    public function __construct(
        protected ?Filesystem $filesystem = null
    ) {
        if (! $this->filesystem) {
            $this->filesystem = new Filesystem;
        }
    }

    /**
     * @param  array<\Smorken\MsGraph\Scripts\Support\File>  $required
     * @param  array<\Smorken\MsGraph\Scripts\Support\File>  $all
     */
    public function remove(array $required, array $all): int
    {
        $removed = 0;
        foreach ($all as $file) {
            if ($this->shouldRemove($file, $required)) {
                $this->filesystem->remove($file->path());
                $removed++;
            }
        }

        return $removed;
    }

    /**
     * @param  array<\Smorken\MsGraph\Scripts\Support\File>  $required
     */
    protected function shouldRemove(File $file, array $required): bool
    {
        foreach ($required as $requiredFile) {
            if ($file->isDir() && $requiredFile->isSameDir($file->dir())) {
                return false;
            }
            if ($file->isFile() && $requiredFile->isSamePath($file->comparable())) {
                return false;
            }
        }

        return true;
    }
}
