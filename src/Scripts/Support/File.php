<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Scripts\Support;

class File
{
    protected string $comparable;

    public function __construct(
        protected string $path,
        protected bool $isFile,
    ) {
        $this->comparable = $this->generateComparable($this->path);
    }

    public function comparable(): string
    {
        return $this->comparable;
    }

    public function dir(): ?string
    {
        if ($this->isDir()) {
            return $this->comparable;
        }

        return dirname($this->comparable);
    }

    public function isDir(): bool
    {
        return ! $this->isFile;
    }

    public function isFile(): bool
    {
        return $this->isFile;
    }

    public function isSameDir(string $dir): bool
    {
        return $dir === $this->dir();
    }

    public function isSamePath(string $path): bool
    {
        return $path === $this->comparable();
    }

    public function path(): string
    {
        return $this->path;
    }

    protected function generateComparable(string $path): string
    {
        return substr($path, strrpos($path, 'src'));
    }
}
