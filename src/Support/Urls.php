<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Support;

use Smorken\MsGraph\Constants\UrlEndpoint;

class Urls
{
    public function __construct(protected array $urls) {}

    public function get(string|UrlEndpoint $url): string
    {
        if (is_a($url, UrlEndpoint::class)) {
            $url = $url->value;
        }

        return $this->urls[$url];
    }
}
