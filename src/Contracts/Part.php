<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Contracts;

interface Part extends \Stringable {}
