<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Contracts;

/**
 * @phpstan-require-extends \Smorken\MsGraph\Request
 */
interface Request
{
    public function client(): Client;

    public function newQuery(): Query;

    public static function setClientResolver(\Closure $resolver): void;
}
