<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Contracts;

/**
 * @phpstan-require-extends \Smorken\MsGraph\Query
 */
interface Query
{
    public function action(): mixed;

    public function getRequest(): Request;
}
