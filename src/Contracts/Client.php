<?php

declare(strict_types=1);

namespace Smorken\MsGraph\Contracts;

use Microsoft\Graph\Beta\GraphServiceClient as BetaClient;
use Smorken\MsGraph\Support\Urls;

interface Client
{
    public function getClient(): BetaClient;

    public function getUrls(): Urls;
}
