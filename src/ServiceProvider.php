<?php

declare(strict_types=1);

namespace Smorken\MsGraph;

use Illuminate\Contracts\Foundation\Application;
use Microsoft\Graph\Beta\GraphServiceClient;
use Smorken\MsGraph\Support\Urls;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->loadConfig();
        $this->handlePublishing();
        Request::setClientResolver(fn () => $this->app[\Smorken\MsGraph\Contracts\Client::class]);
    }

    public function register(): void
    {
        $this->registerClient();
    }

    protected function handlePublishing(): void
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('sm-msgraph'), // @phpstan-ignore function.notFound
        ], 'msgraph-config');
    }

    protected function loadConfig(): void
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php',
            'sm-msgraph'
        );
    }

    protected function registerClient(): void
    {
        $this->app->scoped(\Smorken\MsGraph\Contracts\Client::class, function (Application $app) {
            $config = $app['config']->get('sm-msgraph', []);

            return new Client(
                clientClass: $config['client_class'] ?? GraphServiceClient::class,
                clientId: $config['client_id'],
                clientSecret: $config['client_secret'],
                tenantId: $config['tenant'],
                urls: new Urls($config['urls'] ?? []),
                scopes: $config['scopes'] ?? []
            );
        });
    }
}
