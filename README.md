## Laravel 10+ ms graph package

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

This package is very limited at this time. It only exists as a helper for another package that 
needs to lookup users from Azure AD.
