<?php

declare(strict_types=1);

namespace Tests\Smorken\MsGraph\Unit\Scripts;

use Composer\Composer;
use Composer\Config;
use Composer\IO\BaseIO;
use Composer\Package\RootPackage;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class ComposerTest extends TestCase
{
    #[Test]
    public function it_removes_all_generated_models_except_the_ones__excepted(): void
    {
        $c = new Composer;
        $p = new RootPackage('test', '1.0.0', '1.0.0');
        $p->setConfig(['vendor-dir' => __DIR__.'/']);
        $c->setPackage($p);
        $c->setConfig(new Config(true, __DIR__.'/'));
        $io = m::mock(BaseIO::class);
        $event = new Event(ScriptEvents::PRE_AUTOLOAD_DUMP, $c, $io);
        $io->expects()->write('[microsoft-graph-beta] requires 25 files/directories.');
        $io->expects()->write('[microsoft-graph-beta] has 388 files/directories.');
        $io->expects()->write('[microsoft-graph-beta] removed 338 files/directories.');
        $io->expects()->write('[microsoft-graph-beta] copied 1 files/directories.');
        \Smorken\MsGraph\Scripts\Composer::requiredOnly($event);
        $this->assertTrue(true);
    }

    protected function copyDir(string $source, string $destination): void
    {
        $directory = opendir($source);
        @mkdir($destination);
        while (false !== ($file = readdir($directory))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($source.'/'.$file)) {
                    $this->copyDir($source.'/'.$file, $destination.'/'.$file);
                } else {
                    copy($source.'/'.$file, $destination.'/'.$file);
                }
            }
        }
        closedir($directory);
    }

    protected function setUp(): void
    {
        parent::setUp();
        if (file_exists(__DIR__.'/vendor')) {
            $fs = new Filesystem;
            $fs->remove(__DIR__.'/vendor');
        }
        $this->copyDir(__DIR__.'/vendor.copy/', __DIR__.'/vendor/');
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
