<?php

declare(strict_types=1);

namespace Tests\Smorken\MsGraph\Unit\Query;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\MsGraph\Query\Filter;

class FilterTest extends TestCase
{
    #[Test]
    public function it_creates_a_startsWith_filter(): void
    {
        $sut = new Filter;
        $filter = $sut->startsWith('name', 'test');
        $this->assertEquals("startsWith(name,'test')", (string) $filter);
    }

    #[Test]
    public function it_creates_an_endsWith_filter(): void
    {
        $sut = new Filter;
        $filter = $sut->endsWith('name', 'test');
        $this->assertEquals("endsWith(name,'test')", (string) $filter);
    }

    #[Test]
    public function it_creates_an_equals_filter(): void
    {
        $sut = new Filter;
        $filter = $sut->where('name', 'foo');
        $this->assertEquals("name eq 'foo'", (string) $filter);
    }

    #[Test]
    public function it_creates_an_and_filter_with_startsWith_and_startsWith(): void
    {
        $sut = new Filter;
        $filter = $sut->startsWith('givenName', 'foo')
            ->startsWith('surname', 'bar');
        $this->assertEquals("startsWith(givenName,'foo') and startsWith(surname,'bar')", (string) $filter);
    }
}
