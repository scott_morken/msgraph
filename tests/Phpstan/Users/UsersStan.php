<?php

declare(strict_types=1);

namespace Tests\Smorken\MsGraph\Phpstan\Users;

use Smorken\MsGraph\Query\Filter;
use Smorken\MsGraph\Users\Query;
use Smorken\MsGraph\Users\Users;

$users = new Users;
$q = $users->newQuery();
assert($q->search('12345') instanceof Query);
assert($q->filter(new Filter) instanceof Query);
